"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const datastoreFunctions_1 = __importDefault(require("./datastoreFunctions"));
const fs = __importStar(require("fs"));
function createApplicantArray() {
    return __awaiter(this, void 0, void 0, function* () {
        const raw = yield datastoreFunctions_1.default();
        const res = [];
        for (const rawApp of raw) {
            let pts;
            try {
                pts = rawApp["points"].split(",").map(Number);
            }
            catch (_a) {
                pts = [0, 0, 0, 0];
            }
            const app = {
                first_name: rawApp["first_name"],
                last_name: rawApp["last_name"],
                points: pts,
            };
            res.push(app);
        }
        return res;
    });
}
createApplicantArray().then((val) => {
    const res = [];
    for (const app of val) {
        if (app.points.reduce((a, b) => { return a + b; }, 0) >= 6) {
            res.push(app);
        }
    }
    fs.writeFile("./result.json", JSON.stringify(res), (err) => {
        if (err) {
            console.error(err);
        }
    });
});
